#------------------------------------------------------------------------------
# Description: Configuration for security-related Sysctl
# Author: iDes3rt <ides3rt@protonmail.com>
# Source: https://codeberg.org/ides3rt/Setup
# Last Modified: 2022-10-20
#------------------------------------------------------------------------------
# This configuration is a extension to these links:
#
# https://raw.githubusercontent.com/Kicksecure/security-misc/master/etc/sysctl.d/30_security-misc.conf
# https://raw.githubusercontent.com/Kicksecure/security-misc/master/etc/sysctl.d/30_silent-kernel-printk.conf
#
# So be sure to download those before you use this configuration.
#------------------------------------------------------------------------------

# https://wiki.archlinux.org/title/Sysctl#Enable_TCP_Fast_Open
net.ipv4.tcp_fastopen = 3

# https://wiki.archlinux.org/title/Sysctl#Tweak_the_pending_connection_handling
net.ipv4.tcp_max_syn_backlog = 8192
net.ipv4.tcp_max_tw_buckets = 2048000
net.ipv4.tcp_tw_reuse = 1
net.ipv4.tcp_fin_timeout = 10
net.ipv4.tcp_slow_start_after_idle = 0

# https://wiki.archlinux.org/title/Sysctl#Change_TCP_keepalive_parameters
net.ipv4.tcp_keepalive_time = 60
net.ipv4.tcp_keepalive_intvl = 10
net.ipv4.tcp_keepalive_probes = 6

# https://wiki.archlinux.org/title/Sysctl#Enable_MTU_probing
net.ipv4.tcp_mtu_probing = 1

# https://wiki.archlinux.org/title/Sysctl.html#Enable_BBR
net.core.default_qdisc = cake
net.ipv4.tcp_congestion_control = bbr

# https://wiki.archlinux.org/title/Sysctl#Ignore_ICMP_echo_requests
net.ipv4.icmp_echo_ignore_broadcasts = 1
net.ipv4.icmp_ignore_bogus_error_responses = 1

# https://wiki.archlinux.org/title/Security_features#ptrace_scope
kernel.yama.ptrace_scope = 3

# https://madaidans-insecurities.github.io/guides/linux-hardening.html#boot-kernel
kernel.panic_on_oops = 1

# Use IPv6 Privacy Extensions.
net.ipv6.conf.all.use_tempaddr = 2
net.ipv6.conf.default.use_tempaddr = 2

# https://cisofy.com/lynis/controls/KRNL-6000/
kernel.core_uses_pid = 1
kernel.sysrq = 0
